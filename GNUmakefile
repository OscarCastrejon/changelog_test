GOFMT_FILES?=$$(find . -name '*.go' |grep -v vendor)
TEST?=./...
GOLANGCI_VERSION=v1.35.2
MISSPELL_VERSION=v0.3.4

# test all the existing test files or just one
test: fmtcheck
	@if [ "${name}" = "" ] ; then \
		go test $(TEST) -v -timeout=30s -parallel=4 ; \
	else \
	    go test $(TEST) -v -run $(name) -timeout=30s -parallel=4 ; \
	fi

clean-cache:
	@go clean -cache -modcache -i -r

# check what files need to format
fmtcheck:
	@sh -c "'$(CURDIR)/scripts/gofmtcheck.sh'"

# check if the tools are installed
checktools:
	@sh -c "'$(CURDIR)/scripts/checktools.sh'"

# format all the files with gofmt
fmt:
	@if [ "${pkg}" = "" ] ; then \
		echo "Error: please add a package to be formatted, e.g.:" ; \
		echo "make fmt pkg=kafka" ; \
	else \
		echo "==> Fixing source code with gofmt..." ; \
		gofmt -s -w ./$(pkg) ; \
	fi

# Install dev lints tools 
tools:
	@echo ""
	@echo "==> Installing missing commands dependencies..."
	mkdir -p ./bin/chglog
	curl -Ls https://github.com/git-chglog/git-chglog/releases/download/v0.14.2/git-chglog_0.14.2_linux_amd64.tar.gz | tar -xz -C ./bin/chglog
	mv ./bin/chglog/git-chglog ./bin/ && rm -R ./bin/chglog/

# it runs the lints
lint: fmt
## this is the same that make checktools command to validate the tools
	@if ! sh -c "'$(CURDIR)/scripts/checktools.sh'" 2>&1 /dev/null ; then \
		$(MAKE) -s tools ; \
	fi
	@if [ "${pkg}" = "" ] ; then \
		echo "Error: please add a package to be tested wiht the lint, e.g.:" ; \
		echo "make lint pkg=kafka" ; \
	else \
		echo "" ; \
		$(MAKE) fmt $(pkg) ; \
		echo "==> Checking source code against linters..." ; \
		bin/golangci-lint run ./$(pkg) -v ; \
	fi


# check all the test and run the linter
check: test lint

aws-rie:
	@echo ""
	@echo "==> Installing AWS missing commands dependencies..."
	mkdir -p aws-lambda-rie && curl -Lo aws-lambda-rie/aws-lambda-rie \
	https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie \
	&& chmod +x aws-lambda-rie/aws-lambda-rie

build-lambda:
	docker build -t decoder_lambda .

run-lambda:
	docker run -v $(CURDIR)/aws-lambda-rie:/aws-lambda \
	--entrypoint /aws-lambda/aws-lambda-rie \
	-e AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
	-e AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
	-e AWS_REGION=$(AWS_REGION) \
	-e AWS_BUCKET=$(AWS_BUCKET) \
	-p 9000:8080 decoder_lambda:latest /main

.PHONY: test fmtcheck checktools fmt tools lint check

