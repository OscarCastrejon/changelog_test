package main

import "fmt"

func main() {
	fmt.Println("Version 2")

	suma, resta := operacion(5, 6)

	fmt.Println(suma)
	fmt.Println(resta)
}

func operacion(a, b int) (int, int) {
	return suma(a, b), resta(a, b)
}

func suma(a, b int) int {
	return a + b
}

func resta(a, b int) int {
	return a - b
}
